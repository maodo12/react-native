//import liraries
import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { API_SERVER } from "../../../../assets/constants";
import { useForm } from "react-hook-form";
import CustomInput from "../../../components/customInput/customInput";
import CheckboxButton from "../../../components/radioButton/CheckboxButton";
import CustomButton from "../../../components/customButton/customButton";
import { ScrollView } from "react-native-gesture-handler";

// create a component
const Detail = ({ route, navigation }) => {
  const EMAIL_REGEX =
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  const { item } = route.params;

  const { control, handleSubmit } = useForm({
    defaultValues: {
      username: item.username,
      email: item.email,
      activated: item.activated,
      role: item.role,
      id: item.id,
    },
  });
  const onRetourPressed = () => {
    navigation.navigate("GetUsers");
  };

  const updateData = async (data) => {
    console.log(data);

    // await fetch(API_SERVER + "/users", {
    //   method: "PUT",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify(data),
    // })
    //   .then(() => {
    //     console.log("updated successfully");
    //     navigation.push("GetUsers");
    //   })
    //   .catch((error) => console.warn(error));
  };

  const deleteData = (data) => {
    var myHeaders = new Headers();

    myHeaders.append("Content-Type", "application/json");

    // fetch(API_SERVER + "/users/" + item.id, {
    //   method: "DELETE",
    //   headers: myHeaders,
    //   body: JSON.stringify(data),
    // })
    //   .then(() => {
    //     console.log("deleted successfully ");
    //     navigation.push("GetUsers");
    //   })
    //   .catch((error) => console.log(error));
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Update User</Text>

        <CustomInput
          control={control}
          name="username"
          placeholder="username"
          rules={{ required: "Username is required" }}
        />

        <CustomInput
          control={control}
          name="role"
          placeholder="role"
          rules={{ required: "role is required" }}
        />

        <CustomInput
          control={control}
          name="email"
          placeholder="email"
          rules={{
            required: "Email is required",
            pattern: {
              value: EMAIL_REGEX,
              message: "Email is invalid",
            },
          }}
        />

        <CheckboxButton control={control} name="activated" title="Active" />

        <CustomButton
          text="Update"
          onPress={handleSubmit(updateData)}
          type="PRIMARY"
        />
        <CustomButton
          text="Delete"
          onPress={handleSubmit(deleteData)}
          fgColor="white"
          bgColor="#DD4D44"
        />
        <CustomButton
          text="Retour"
          onPress={onRetourPressed}
          fgColor="#363636"
          bgColor="#E3E3E3"
        />
      </View>
    </ScrollView>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

//make this component available to the app
export default Detail;
