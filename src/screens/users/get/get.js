import React, { useState, useEffect } from "react";
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  TouchableOpacity,
} from "react-native";
import {useNavigation} from '@react-navigation/native';



const marginBottomItem = 20;
const paddingItem = 10;
const imgHeight = 100;
const sizeOfItem = imgHeight + paddingItem * 2 + marginBottomItem;

const BASE_URL = "http://localhost:8080/api";

const GetUsers = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsloading] = useState(false);
  const Yscroll = React.useRef(new Animated.Value(0)).current;
  const navigation = useNavigation();

  useEffect(() => {
    setIsloading(true);
    getAllUsers();
    return () => {};
  }, []);

  const getAllUsers = () => {
    fetch(`${BASE_URL}/users`)
      .then((res) => res.json())
      .then((resJson) => {
        console.log(resJson);
        setData(resJson);
      })
      .catch(console.error)
      .finally(() => setIsloading(false));
  };

  const renderUser = ({ item, index }) => {
    const scale = Yscroll.interpolate({
      inputRange: [-1, 0, sizeOfItem * index, sizeOfItem * (index + 2)],
      outputRange: [1, 1, 1, 0],
    });
    return (
      <TouchableOpacity onPress={()=>navigation.navigate('Detail', {
        item: item
      })}>
      <Animated.View
        style={[
          styles.item,
          {
            transform: [{ scale }],
          },
        ]}
      >
        <Image
          style={styles.image}
          source={{ uri: item.picture }}
          resizeMode="contain"
          contentContainerStyle={{ padding: 20 }}
        />
        <View style={styles.wrapText}>
          <Text
            style={styles.fontSize}
          >{`${item.id}`}</Text>
          <Text style={styles.fontSize}>{`${item.username}`}</Text>
          <Text style={styles.fontSize}>{`${item.email}`}</Text>
          <Text style={styles.fontSize}>{`${item.role}`}</Text>
          <Text style={styles.fontSize}>{`${item.createdDate}`}</Text>
          <Text style={styles.fontSize}>{`${item.lastModifiedDate}`}</Text>
        </View>
      </Animated.View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require("../../../../assets/images/ModelS.jpeg")}
        style={StyleSheet.absoluteFillObject}
        blurRadius={80}
      />
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <Animated.FlatList
          data={data}
          keyExtractor={(item) => `key-${item.id}`}
          renderItem={renderUser}
          contentContainerStyle={{
            padding: 20,
          }}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: Yscroll } } }],
            { useNativeDriver: true }
          )}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  fontSize: {
    fontSize: 18,
  },
  image: {
    width: 100,
    height: imgHeight,
  },
  wrapText: {
    flex: 1,
    marginLeft: 10,
    justifyContent: "center",
  },
  item: {
    flexDirection: "row",
    marginBottom: marginBottomItem,
    borderRadius: 10,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.3,
    shadowRadius: 30,
    padding: paddingItem,
  },
  container: {
    flex: 1,
  },
});

export default GetUsers;
