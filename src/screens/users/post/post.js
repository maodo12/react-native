import { View, StyleSheet, Image, ScrollView, Text } from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import CustomButton from "../../../components/customButton/customButton";
import { useForm } from "react-hook-form";
import CheckboxButton from "../../../components/radioButton/CheckboxButton";
import { API_SERVER } from "../../../../assets/constants";
import {useNavigation} from '@react-navigation/native';

const PostUser = () => {
  const navigation = useNavigation();
  const { control, handleSubmit, watch } = useForm();
  const pwd = watch("password");

  const EMAIL_REGEX =
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


  const onRegisterPressed = async (data) => {
    console.log(data);
    // await fetch(API_SERVER+'/users', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify(data)
    // });
    console.warn("Register pressed");
    navigation.navigate("GetUsers");
  };

  const onGetPressed = async () => {
    navigation.navigate("GetUsers");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Create A New User</Text>

        <CustomInput
          control={control}
          name="username"
          placeholder="username"
          rules={{ required: "Username is required" }}
        />

        <CustomInput
          control={control}
          name="email"
          placeholder="email"
          rules={{
            required: "Email is required",
            pattern: {
              value: EMAIL_REGEX,
              message: "Email is invalid",
            },
          }
        }
        />

        <CustomInput
          control={control}
          name="role"
          placeholder="role"
          rules={{ required: "Role is required" }}
        />
        <CustomInput
          control={control}
          name="password"
          placeholder="password"
          rules={{
            required: "Password is required",
            minLength: {
              value: 3,
              message: "Password should minimum 3 characters long",
            },
          }}
          secureTextEntry
        />
        <CustomInput
          control={control}
          name="repeat-password"
          placeholder="Repeat password"
          rules={{
            required: "Repeat Password is required",
            validate: value => value === pwd || "Password do not match"
    
          }}
          secureTextEntry
        />
        <CheckboxButton
        control={control}
        name="activated"
        title="Active"
        />
        <CustomButton
          text="Register"
          onPress={handleSubmit(onRegisterPressed)}
          type="PRIMARY"
        />
        <CustomButton
          text="Get User"
          onPress={onGetPressed}
          type="PRIMARY"
        />

      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

export default PostUser;
