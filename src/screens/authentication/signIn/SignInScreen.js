import React, { useContext } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  useWindowDimensions,
  Text,
} from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import Logo from "../../../../assets/images/logo.png";
import CustomButton from "../../../components/customButton/customButton";
import SocialSignInButtons from "../../../components/SocialSignInButtons";

import { useNavigation } from "@react-navigation/native";
import { useForm } from "react-hook-form";

const SignInScreen = () => {
  const { control, handleSubmit } = useForm();

  const { height } = useWindowDimensions();

  const navigation = useNavigation();

  const onSignInPressed = async (data) => {
    console.log(data);

    console.warn("Register pressed");
    navigation.navigate("GetUsers");
  };
  const onAddUserPressed = () => {
    //validate user
    navigation.navigate("PostUser");
  };

  const onForgotPasswordPressed = () => {
    console.log("Forgot Password pressed");
    navigation.navigate("ForgotPassword");
  };

  const onDHAccountPressed = () => {
    console.warn("Don't have an account pressed");
    navigation.navigate("SignUp");
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Image
          source={Logo}
          style={[styles.img, { height: height * 0.3 }]}
          resizeMode="contain"
        />

        <CustomInput
          control={control}
          name="username"
          placeholder="username"
          rules={{ required: "Username is required" }}
        />
        <CustomInput
          control={control}
          name="password"
          placeholder="password"
          rules={{
            required: "Password is required",
            minLength: {
              value: 8,
              message: "Password should minimum 8 characters long",
            },
          }}
          secureTextEntry
        />
        <CustomButton
          text="Add User"
          onPress={onAddUserPressed}
          type="PRIMARY"
        />
        <CustomButton
          text="Sign In"
          onPress={handleSubmit(onSignInPressed)}
          type="PRIMARY"
        />
        <CustomButton
          text="Forgot password"
          onPress={onForgotPasswordPressed}
          type="TERTIARY"
        />
        <SocialSignInButtons />
        <CustomButton
          text="Don't have an account? create one"
          onPress={onDHAccountPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  img: {
    width: "70%",
    maxWidth: 300,
    maxHeight: 200,
  },
});

export default SignInScreen;
