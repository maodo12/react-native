import { View, StyleSheet, Image, ScrollView, Text } from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import CustomButton from "../../../components/customButton/customButton";
import { useForm } from "react-hook-form";

import {useNavigation} from '@react-navigation/native';

const ForgotPasswordScreen = () => {
  const { control, handleSubmit } = useForm();

  const EMAIL_REGEX =
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  const onSendPressed = () => {
    console.warn("Send pressed");
    navigation.navigate('NewPassword')
  };

  const navigation = useNavigation();

  const onBTSPressed = () => {
    console.warn("Back to sign In pressed");
    navigation.navigate('SignIn')
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Reset your username</Text>
        <CustomInput
          control={control}
          name="email"
          placeholder="email"
          rules={{
            required: "Email is required",
            pattern: {
              value: EMAIL_REGEX,
              message: "Email is invalid",
            },
          }
        }
        />
        <CustomButton
          text="Send"
          onPress={handleSubmit(onSendPressed)}
          type="PRIMARY"
        />
        <CustomButton
          text="Back to sign In"
          onPress={onBTSPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

export default ForgotPasswordScreen;
