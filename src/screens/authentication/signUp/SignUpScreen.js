import { View, StyleSheet, Image, ScrollView, Text } from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import CustomButton from "../../../components/customButton/customButton";
import SocialSignInButtons from "../../../components/SocialSignInButtons/SocialSignInButtons";
import { useNavigation } from "@react-navigation/native";
import { useForm } from "react-hook-form";
import RadioButton from "../../../components/radioButton/CheckboxButton";

const SignUpScreen = () => {
  const { control, handleSubmit, watch } = useForm();
  const pwd = watch("password");

  const EMAIL_REGEX =
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  const navigation = useNavigation();

  const onRegisterPressed = async (data) => {
    console.warn(data);
    // await fetch('http://localhost:8080/api/patients', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify(data)
    // });
    console.warn("Register pressed");
    navigation.navigate("ConfirmEmail");
  };

  const onPPPressed = () => {
    console.warn("Private policy pressed");
  };

  const onTOUPressed = () => {
    console.warn("Term of use pressed");
  };

  const onForgotPasswordPressed = () => {
    console.warn("Forgot Password pressed");
    navigation.navigate("ForgotPassword");
  };

  const onHaveAccountPressed = () => {
    console.warn("have an account pressed");
    navigation.navigate("SignIn");
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Create A New Account</Text>

        <CustomInput
          control={control}
          name="username"
          placeholder="username"
          rules={{ required: "Username is required" }}
        />

        <CustomInput
          control={control}
          name="email"
          placeholder="email"
          rules={{
            required: "Email is required",
            pattern: {
              value: EMAIL_REGEX,
              message: "Email is invalid",
            },
          }
        }
        />
        <CustomInput
          control={control}
          name="password"
          placeholder="password"
          rules={{
            required: "Password is required",
            minLength: {
              value: 3,
              message: "Password should minimum 3 characters long",
            },
          }}
          secureTextEntry
        />
        <CustomInput
          control={control}
          name="repeat-password"
          placeholder="Repeat password"
          rules={{
            required: "Repeat Password is required",
            validate: value => value === pwd || "Password do not match"
    
          }}
          secureTextEntry
        />
        <CustomButton
          text="Register"
          onPress={handleSubmit(onRegisterPressed)}
          type="PRIMARY"
        />
        <Text style={styles.text} onPress={onTOUPressed}>
          By reistering, you confirm that you accept our{" "}
          <Text style={styles.link}>Terms of use </Text>and{" "}
          <Text style={styles.link} onPress={onPPPressed}>
            Privacy Policy
          </Text>
        </Text>
        <CustomButton
          text="Forgot password"
          onPress={onForgotPasswordPressed}
          type="TERTIARY"
        />
        <SocialSignInButtons />
        <CustomButton
          text="Have an account? sign In"
          onPress={onHaveAccountPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

export default SignUpScreen;
