import { View, StyleSheet, Image, ScrollView, Text } from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import CustomButton from "../../../components/customButton/customButton";
import { useForm } from "react-hook-form";

import {useNavigation} from '@react-navigation/native'
const ConfirmEmailScreen = () => {
  const { control, handleSubmit } = useForm();

  const navigation = useNavigation();

  const onConfirmPressed = () => {
    console.warn("Confirm pressed");
    navigation.navigate('Home')
  };
  const onRSPressed = () => {
    console.warn("Resend code pressed");
  };
  const onBTSPressed = () => {
    console.warn("Back to sign In pressed");
    navigation.navigate('SignIn')
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Confirm your email</Text>

        <CustomInput
          control={control}
          name="code"
          placeholder="Confirmation code"
          rules={{ required: "Code is required" }}
        />
        <CustomButton
          text="Confirm"
          onPress={handleSubmit(onConfirmPressed)}
          type="PRIMARY"
        />
        <CustomButton
          text="resend code"
          onPress={onRSPressed}
          type="SECONDARY"
        />
        <CustomButton
          text="Back to sign In"
          onPress={onBTSPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

export default ConfirmEmailScreen;
