import { View, StyleSheet, Image, ScrollView, Text } from "react-native";
import CustomInput from "../../../components/customInput/customInput";
import CustomButton from "../../../components/customButton/customButton";
import { useForm } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";

const NewPasswordScreen = () => {
  const { control, handleSubmit } = useForm();

  const navigation = useNavigation();

  const onSendPressed = () => {
    console.warn("Send pressed");
    navigation.navigate("Home");
  };

  const onBTSPressed = () => {
    console.warn("Back to sign In pressed");
    navigation.navigate("SignIn");
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Text style={styles.title}>Reset your password</Text>
        <CustomInput
          control={control}
          name="code"
          placeholder="Confirmation code"
          rules={{ required: "Code is required" }}
        />
        <CustomInput
          control={control}
          name="password"
          placeholder="password"
          rules={{
            required: "Password is required",
            minLength: {
              value: 8,
              message: "Password should minimum 8 characters long",
            },
          }}
          secureTextEntry
        />
        <CustomButton
          text="Send"
          onPress={handleSubmit(onSendPressed)}
          type="PRIMARY"
        />
        <CustomButton
          text="Back to sign In"
          onPress={onBTSPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 10,
  },
});

export default NewPasswordScreen;
