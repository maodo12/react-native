import CarsList from "../../components/carsList";
import Header from "../../components/header";

const HomeScreen = () => {
  return (
    <>
    <Header/>
    <CarsList/>
    </>
  );
};

export default HomeScreen;
