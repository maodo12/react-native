import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ConfirmEmailScreen from "../screens/authentication/confirmEmail/ConfirmEmail";
import ForgotPasswordScreen from "../screens/authentication/forgotPassword/forgotPasswordScreen";
import NewPasswordScreen from "../screens/authentication/newPassword/NewPasswordScreen";
import SignUpScreen from '../screens/authentication/signUp/SignUpScreen';
import SignInScreen from '../screens/authentication/signIn/SignInScreen';
import HomeScreen from "../screens/home/HomeScreen";
import PostUser from "../screens/users/post/post";
import GetUsers from "../screens/users/get/get";
import Detail from "../screens/users/detail/Detail";

const Stack = createStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SignIn" component={SignInScreen}/>
        <Stack.Screen name="SignUp" component={SignUpScreen}/>
        <Stack.Screen name="ConfirmEmail" component={ConfirmEmailScreen}/>
        <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen}/>
        <Stack.Screen name="NewPassword" component={NewPasswordScreen}/>
        <Stack.Screen name="Home" component={HomeScreen}/>
        <Stack.Screen name="PostUser" component={PostUser}/>
        <Stack.Screen name="GetUsers" component={GetUsers}/>
        <Stack.Screen name="Detail" component={Detail}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
