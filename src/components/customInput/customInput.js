import { View, StyleSheet, TextInput, Text } from "react-native";
import { Controller } from "react-hook-form";

const CustomInput = ({ control, name, placeholder, rules = {}, secureTextEntry }) => {
  return (
    
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({field: { value = '', onChange, onBlur }, fieldState:{error}}) => (
          <>
          <View style={[styles.container, {borderColor: error? "red" : '#E8E8E8'}]}>
          <TextInput
            placeholder={placeholder}
            value={value}
            onChangeText={onChange}
            onBlur={onBlur}
            secureTextEntry={secureTextEntry}
            style={styles.input}
          />
          </View>
          {error &&(
            <Text style={{color:"red", alignSelf:"stretch"}}>{error.message || "Error"}</Text>
          )
          }
          </>
        )}
      />
    
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: "100%",

    borderColor: "#e8e8e8",
    borderWidth: 1,
    borderRadius: 5,

    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
  },
  input: {},
});
export default CustomInput;
