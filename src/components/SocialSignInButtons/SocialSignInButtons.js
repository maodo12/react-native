import CustomButton from "../customButton/customButton";

const SocialSignInButtons = () => {

  const onFacebookPressed = () => {
    console.warn("Sign In with facebook pressed");
  };

  const onGooglePressed = () => {
    console.warn("Sign In with google pressed");
  };

  const onApplePressed = () => {
    console.warn("Sign In with apple pressed");
  };

  return (
      <>
        <CustomButton
          text="Sign In with Facebook"
          onPress={onFacebookPressed}
          fgColor="#4765A9"
          bgColor="#E7EAF4"
        />
        <CustomButton
          text="Sign In wuth google"
          onPress={onGooglePressed}
          fgColor="#DD4D44"
          bgColor="#FAE9EA"
        />
        <CustomButton
          text="Sign In with apple"
          onPress={onApplePressed}
          fgColor="#363636"
          bgColor="#E3E3E3"
        />
      </>
  );
};

export default SocialSignInButtons;
