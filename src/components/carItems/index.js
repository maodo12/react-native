import { ImageBackground, Text, View } from "react-native";
import StyledButton from "../styledButton";
import styles from "./styles";

const CarItems = (props) => {
  const { name, tagline, taglineCTA, image } = props.car;
  return (

    <View style={styles.carContainer}>
      <ImageBackground source={image} style={styles.image} />

      <View style={styles.titles}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.subtitle}>
          {tagline}{" "}
          <Text style={styles.subtitleCTA}>{taglineCTA}</Text>
        </Text>
        
      </View>
      <View style={styles.buttonsContainer}>
        <StyledButton
          type="primary"
          content="Customer order"
          onPress={() => {
            console.warn("Customer order was pressed");
          }}
        />

        <StyledButton
          type="secondary"
          content="Existing Inventory"
          onPress={() => {
            console.warn("Existing Inventory was pressed");
          }}
        />
      </View>
    </View>
  );
};

export default CarItems;
