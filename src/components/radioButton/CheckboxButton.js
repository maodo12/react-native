import { View, StyleSheet, CheckBox, Text } from "react-native";
import { Controller } from "react-hook-form";

const CheckboxButton = ({ control, name, rules = {}, title }) => {
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field: { value = "", onChange } }) => (
        <>
          <View style={[styles.container, styles.checkboxContainer]}>
              <CheckBox
                value={value}
                onValueChange={onChange}
                style={styles.checkbox}
              />
              <Text style={styles.label}> {title} </Text>
          </View>
        </>
      )}
    />
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "stretch",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});
export default CheckboxButton;
